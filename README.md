# Friendships

## Overview

App outlining friendships between three users, built using Django, REST API and Aloe to demonstrate the behavior driven development model and testing with Aloe



## Installation

Git clone/ fork repository

```

$ pipenv shell
$ pipenv install

```


To run the tests using Aloe on the app from command line:

```
python manage.py harvest 

```

## Output

```
nosetests --verbosity=1
Creating test database for alias 'default'...
......
----------------------------------------------------------------------
Ran 6 tests in 5.054s

OK
Destroying test database for alias 'default'...

```

## Resources


https://github.com/testdrivenio/django-aloe-bdd

https://testdriven.io/blog/behavior-driven-development-with-django-and-aloe/

